var keystone = require('keystone');

exports = module.exports = function(req, res) {
	
	var view = new keystone.View(req, res),
		locals = res.locals;
	
	// Used for currently selected item in navigation
	locals.section = 'collections';

	locals.data = {
		title: 'Collections'
	};

	// Load all published Collections
	view.on('init', function(next) {
		
		var q = keystone.list('Collection').model.find({
			state: 'published'
		});
		
		q.exec(function(err, result) {
			locals.data.collectionList = result;

			if (err) return res.status(500).render('errors/500');

			next(err);
		});
		
	});
	
	// Render the view
	view.render('collections');
	
};
