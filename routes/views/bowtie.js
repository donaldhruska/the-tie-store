var keystone = require('keystone');

exports = module.exports = function(req, res) {
	
	var view = new keystone.View(req, res),
		locals = res.locals;
	
	// Used for currently selected item in navigation
	locals.section = 'bowties';

	locals.filters = {
		tie: req.params.bowtieid
	};
	locals.data = {};

	// Load the current Bow Tie
	view.on('init', function(next) {
		
		var q = keystone.list('Tie').model.findOne({
			state: 'published',
			type: 'bow tie',
			slug: locals.filters.tie
		}).populate('material pattern primaryColors tertiaryColor');
		
		q.exec(function(err, result) {
			locals.data.tie = result;
			
			if (err) return res.status(500).render('errors/500');
        	if (!result) return res.status(404).render('errors/404');

			next(err);
		});
		
	});
	
	// Render the view
	view.render('tie');
	
};
