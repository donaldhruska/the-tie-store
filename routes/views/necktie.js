var keystone = require('keystone');

exports = module.exports = function(req, res) {
	
	var view = new keystone.View(req, res),
		locals = res.locals;
	
	// Used for currently selected item in navigation
	locals.section = 'neckties';

	locals.filters = {
		tie: req.params.necktieid
	};
	locals.data = {};

	// Load the current Necktie
	view.on('init', function(next) {
		
		var q = keystone.list('Tie').model.findOne({
			state: 'published',
			type: 'necktie',
			slug: locals.filters.tie
		}).populate('material pattern primaryColors tertiaryColor');
		
		q.exec(function(err, result) {
			locals.data.tie = result;
			if (err) return res.status(500).render('errors/500');
        	if (!result) return res.status(404).render('errors/404');

			var relatedProductsQuery = keystone.list('PocketSquare').model.find({
				state: 'published',
				primaryColors: locals.data.tie.tertiaryColor
			});

			relatedProductsQuery.exec(function(err2, result2) {
				locals.data.relatedProducts = result2;
				if (err2) return res.status(500).render('errors/500');
	        	if (!result2) return res.status(404).render('errors/404');
			});

			next(err);
		});
		
	});
	
	// Render the view
	view.render('tie');
	
};
