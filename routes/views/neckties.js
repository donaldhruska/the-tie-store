var keystone = require('keystone');

exports = module.exports = function(req, res) {
	
	var view = new keystone.View(req, res),
		locals = res.locals;
	
	// Used for currently selected item in navigation
	locals.section = 'neckties';

	locals.data = {
		title: 'Neckties'
	};

	// Load all published Neckties
	view.on('init', function(next) {
		
		var q = keystone.list('Tie').model.find({
			type: 'necktie',
			state: 'published'
		});
		
		q.exec(function(err, result) {
			locals.data.tieList = result;

			if (err) return res.status(500).render('errors/500');

			next(err);
		});
		
	});
	
	// Render the view
	view.render('ties');
	
};
