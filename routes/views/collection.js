var keystone = require('keystone');

exports = module.exports = function(req, res) {
	
	var view = new keystone.View(req, res),
		locals = res.locals;
	
	// Used for currently selected item in navigation
	locals.section = 'collections';

	locals.filters = {
		collection: req.params.collectionid
	};
	locals.data = {};

	// Load the current Collection
	view.on('init', function(next) {
		
		var q = keystone.list('Collection').model.findOne({
			state: 'published',
			slug: locals.filters.collection
		}).populate('tie pocketSquare');
		
		q.exec(function(err, result) {
			locals.data.collection = result;
			if (err) return res.status(500).render('errors/500');
        	if (!result) return res.status(404).render('errors/404');

			next(err);
		});
		
	});
	
	// Render the view
	view.render('collection');
	
};
