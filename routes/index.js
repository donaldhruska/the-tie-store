var keystone = require('keystone'),
	middleware = require('./middleware'),
	importRoutes = keystone.importer(__dirname);

// Common Middleware
keystone.pre('routes', middleware.initLocals);
keystone.pre('render', middleware.flashMessages);

// Import Route Controllers
var routes = {
	views: importRoutes('./views')
};

// Setup Route Bindings
exports = module.exports = function(app) {
	
	// Views
	app.get('/', routes.views.index);
	app.get('/collections', routes.views.collections);
	app.get('/collections/:collectionid', routes.views.collection);
	app.get('/neckties', routes.views.neckties);
	app.get('/neckties/:necktieid', routes.views.necktie);
	app.get('/bowties', routes.views.bowties);
	app.get('/bowties/:bowtieid', routes.views.bowtie);
	
	app.get('/errors/404', routes.views.errors.notfound);
	app.get('/errors/500', routes.views.errors.error);
};
