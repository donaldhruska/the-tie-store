var keystone = require('keystone');

// Material model
var Material = new keystone.List('Material', {
	singular: 'Material',
	plural: 'Materials',
    autokey: { path: 'slug', from: 'name', unique: true },
	map: { name: 'name' }
});

Material.add({
	name: { type: String, required: true, index: true }
});

// Registration
Material.defaultColumns = 'name';
Material.register();
