var keystone = require('keystone'),
	Types = keystone.Field.Types;

// Collection model
var Collection = new keystone.List('Collection', {
	singular: 'Collection',
	plural: 'Collections',
    autokey: { path: 'slug', from: 'name', unique: true },
	map: { name: 'name' }
});

Collection.add({
	name: { type: String, required: true, index: true },
	tie: { type: Types.Relationship, ref: 'Tie', many: true },
	pocketSquare: { type: Types.Relationship, ref: 'PocketSquare', many: true },
	image: { type: Types.CloudinaryImage, autoCleanup: true },
    state: { type: Types.Select, options: 'draft, published, archived', default: 'draft', index: true }
});

// Registration
Collection.defaultColumns = 'name, state|20%';
Collection.register();
