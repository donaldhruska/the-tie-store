var keystone = require('keystone'),
	Types = keystone.Field.Types;

// PocketSquare model
var PocketSquare = new keystone.List('PocketSquare', {
	singular: 'Pocket Square',
	plural: 'Pocket Squares',
    autokey: { path: 'slug', from: 'name', unique: true },
	map: { name: 'name' }
});

PocketSquare.add({
	name: { type: String, required: true, index: true },
	price: { type: Types.Money, required: true, initial: true, format: '$0,0.00' },
	primaryColors: { type: Types.Relationship, ref: 'Color', many: true },
	tertiaryColor: { type: Types.Relationship, ref: 'Color' },
	pattern: { type: Types.Relationship, ref: 'Pattern' },
	material: { type: Types.Relationship, ref: 'Material' },
	images: { type: Types.CloudinaryImages, autoCleanup: true },
    state: { type: Types.Select, options: 'draft, published, archived', default: 'draft', index: true }
});

// Registration
PocketSquare.defaultColumns = 'name, state|20%';
PocketSquare.register();
