var keystone = require('keystone');

// Pattern model
var Pattern = new keystone.List('Pattern', {
	singular: 'Pattern',
	plural: 'Patterns',
    autokey: { path: 'slug', from: 'name', unique: true },
	map: { name: 'name' }
});

Pattern.add({
	name: { type: String, required: true, index: true }
});

// Registration
Pattern.defaultColumns = 'name';
Pattern.register();
