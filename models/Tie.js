var keystone = require('keystone'),
	Types = keystone.Field.Types;

// Tie model
var Tie = new keystone.List('Tie', {
	singular: 'Tie',
	plural: 'Ties',
    autokey: { path: 'slug', from: 'name', unique: true },
	map: { name: 'name' }
});

Tie.add({
	name: { type: String, required: true, index: true },
	type: { type: Types.Select, options: 'necktie, bow tie', required: true, initial: true },
	price: { type: Types.Money, required: true, initial: true, format: '$0,0.00' },
	primaryColors: { type: Types.Relationship, ref: 'Color', many: true },
	tertiaryColor: { type: Types.Relationship, ref: 'Color' },
	pattern: { type: Types.Relationship, ref: 'Pattern' },
	material: { type: Types.Relationship, ref: 'Material' },
	images: { type: Types.CloudinaryImages, autoCleanup: true },
    state: { type: Types.Select, options: 'draft, published, archived', default: 'draft', index: true }
});

// Registration
Tie.defaultColumns = 'name, type|20%, state|20%';
Tie.register();
