var keystone = require('keystone'),
	Types = keystone.Field.Types;

// Color model
var Color = new keystone.List('Color', {
	singular: 'Color',
	plural: 'Colors',
    autokey: { path: 'slug', from: 'name', unique: true },
	map: { name: 'name' }
});

Color.add({
	name: { type: String, required: true, index: true },
	colorValue: { type: Types.Color }
});

// Registration
Color.defaultColumns = 'name';
Color.register();
