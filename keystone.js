// Simulate config options from your prod environment using .env file
require('dotenv').load();

// Require keystone
var keystone = require('keystone'),
	handlebars = require('express-handlebars');

keystone.init({

	'name': 'The Tie Store',
	'brand': 'The Tie Store',
	
	'sass': 'public',
	'static': 'public',
	'favicon': 'public/favicon.ico',
	'views': 'templates/views',
	'view engine': 'hbs',
	
	'custom engine': handlebars.create({
		layoutsDir: 'templates/views/layouts',
		partialsDir: 'templates/views/partials',
		defaultLayout: 'default',
		helpers: new require('./templates/views/helpers')(),
		extname: '.hbs'
	}).engine,
	
	'auto update': true,
	'session': true,
	'auth': true,
	'user model': 'User',
	'cookie secret': 'dDV$A$WqrzU+xkPiND2"`K;9ULZs[W#=y,th+-q2c(Xzc[|fW4S,$K8"kT+dK99c'

});

keystone.import('models');

keystone.set('locals', {
	_: require('underscore'),
	env: keystone.get('env'),
	utils: keystone.utils,
	editable: keystone.content.editable
});

keystone.set('routes', require('./routes'));

keystone.set('nav', {
	'products': ['ties', 'pocket-squares', 'collections'],
	'attributes': ['colors', 'patterns', 'materials'],
	'users': 'users'
});

keystone.start();
